/* Sensor pin configurations and libraries */
#include "aw_clock.h"

/* I2C subsystems */
RTC_DS1307 rtc;
LiquidCrystal_SR lcd(4, 6, 5); // Pin 4 - Data Enable/ SER, Pin 6 - Clock/SRCLK, Pin 5 - SCL/RCLK


/* STRING CONSTANTS */
const char* factoryResetText = "   Factory Reset?   ";

const char daysOfTheWeek_Short[7][4] = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
const char* daysOfTheWeek_Letter = "SMTWHFS";

const char months[12][4] = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

const char color_names[8][7] = {"White", "Red", "Green", "Blue", "Violet", "Cyan", "Yellow", "Off"};

const char dateFormats[6][21] = {"   %s %02d/%02d/%04d   ", "   %s %04d/%02d/%02d   ", "   %s %02d/%02d/%04d   ", "   %s %04d-%02d-%02d   ", " %s %s %d, %04d  ", " %s %s %d, %04d  "};


/* OTHER CONSTANT ARRAYS */
const uint8_t colors[8][3] = {{0xff, 0x77, 0xff}, {0xff, 0x00, 0x00}, {0x00, 0xff, 0x00}, {0x00, 0x00, 0xff}, {0xff, 0x00, 0xff}, {0x00, 0xff, 0xff}, {0xff, 0x77, 0x00}, {0x00, 0x00, 0x00}};

const int sleepTimePreset[9] = {0, 5, 10, 15, 30, 60, 120, 300, 600};

const int fsr_oz_const = 15;

/* STATE VARAIABLES */
int hour, minute = 1, second, month, day, year; // minute = 1 to avoid alarm going off on start
int alarmHr = 0;
int alarmMin = 0;
int waterThreshold = 8;
int backlightColor = 2;

/**
 * Time that the clock stays on in seconds; 0 = never sleep
 */
int wakeTime = 0;

/**
 * bit mask for alarm days of week
 * 0b10      - monday
 * 0b100     - tuesday
 * 0b1000    - wednesday
 * 0b10000   - thursday
 * 0b100000  - friday
 * 0b1000000 - saturday
 * 0b1       - sunday
 */
uint8_t alarmDays_mask = 0;
int alarmDays_set = 0;

bool alarmState = false;
bool alarmDisabled = false;
int fsrValAtAlarm = 0;
bool time12hr = true;
bool showWeekday = true;

/**
 * 0 = MM/DD/YYYY
 * 1 = YYYY/MM/DD
 * 2 = DD/MM/YYYY
 * 3 = ISO 8061 (YYYY-MM-DD)
 * 4 = Month D, YYYY
 * 5 = D Month, YYYY
 */
int dateFmt = 0;

int clockRefreshCounter = 0;
int secSinceLastEnter = 0;
int timeAwake = 0;

int factoryResetSelection = 0;

/**
 * 0 = not changing settings
 * 1 = hour
 * 2 = minute
 * 3 = second
 * 4 = 12/24h time
 * 5 = day
 * 6 = month
 * 7 = year
 * 8 = show weekday (y/n)
 * 9 = date format
 * 10 = sleep time
 *  
 * 11 = alarm hour
 * 12 = alarm minute
 * 13 = alarm days toggle
 * 14 = water threshold (default 8oz)
 * 
 * 127 = factory reset (y/n)
 */
int settingChange = 0;

// Pressed/released flags (time in 1/10ths of a second the button has been held)
int enter_lock = 0;
int plus_lock = 0;
int minus_lock = 0;

int belowThresholdCounter = 0;

/**
 * Arduino setup function.
 */
void setup() {
  Serial.begin(57600);
  Serial.println("RTC Test");

  pinMode(PLUS_BTN, INPUT_PULLUP);
  pinMode(MINUS_BTN, INPUT_PULLUP);
  pinMode(ENTER_BTN, INPUT_PULLUP);

  pinMode(BUZZER, OUTPUT);

  tone(BUZZER, 800, 100);

  lcd.begin(20, 4);
  lcd.home();

  if (!rtc.begin()) {
    Serial.println("[FATAL] Failed to init RTC!");
    Serial.end();
    while (1) delay(10);
  }

  if (!rtc.isrunning()) {
    Serial.println("[WARN] RTC is NOT running, setting time to factory default...");
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
  
  Serial.println("[INFO] RTC ready.");
}

/**
 * Arduino loop function
 */
void loop() {
  delay(100);
  clockRefreshCounter++;

  int fsrVal = analogRead(FSR);
  
//  Serial.print(fsrVal); // debug

  int dow_bitmask = 1 <<  rtc.now().dayOfTheWeek();

  if (hour == alarmHr && minute == alarmMin /** && (alarmDays_mask & dow_bitmask == dow_bitmask)**/ && !alarmState && !alarmDisabled) {
    alarmState = true;
    fsrValAtAlarm = fsrVal;
  }

  if (minute == alarmMin + 1 || (alarmMin + 1 >= 59 && minute == 0)) {
    alarmDisabled = false;
  }

  int w_check_val = fsrValAtAlarm - (waterThreshold * fsr_oz_const);
//  Serial.print(" "); // debug
//  Serial.println(w_check_val); // debug

  if (alarmState && ((fsrVal > 20 && fsrVal < w_check_val) || (fsrVal < 20 && w_check_val < 20))) {
    belowThresholdCounter++;
    if (belowThresholdCounter > 29) {
      alarmState = false;
      alarmDisabled = true;
    }
  } else {
    belowThresholdCounter = 0;
  }

  if (alarmState && clockRefreshCounter < 5) {
    tone(BUZZER, 800);
  } else {
    noTone(BUZZER);
  }

  // Check to see if user wakes up clock, handle button presses
  handleButtons();

  if (clockRefreshCounter % 2 == 0) { // Only poll clock every 1/5 second
    if (clockRefreshCounter >= 10) {
      clockRefreshCounter = 0;
    }
    secSinceLastEnter++;
    
    if (wakeTime <= 0 || secSinceLastEnter < wakeTime) { // Only show and update display while clock is awake
      timeAwake++;
      
      updateDisplay();
    
    } else if (secSinceLastEnter == wakeTime) {
      Serial.println("Sleep mode...");
      settingChange = 0;
      timeAwake = 0;
      lcd.off();
    }
  }
}

void changeSetting(int setting, int delta) {
//  Serial.print(delta);
	switch(setting) {
    case 11:
      alarmHr += delta;
//      Serial.print("alhr_d:");
//      Serial.println(delta);
      if (alarmHr < 0) {
        alarmHr = 23;
      } else if (alarmHr > 23) {
        alarmHr = 0;
      }
      alarmState = false;
      alarmDisabled = false;
      return;
    case 12:
      alarmMin += delta;
      if (alarmMin < 0) {
        alarmMin = 59;
      } else if (alarmMin > 59) {
        alarmMin = 0;
      }
      alarmState = false;
      alarmDisabled = false;
      return;
    case 13:
      if (delta < 0) {
        alarmDays_mask &= (127 - (1 << alarmDays_set));
      } else if (delta > 0) {
        alarmDays_mask |= 1 << alarmDays_set;
      }
      Serial.println(alarmDays_mask);
      alarmDays_set++;
      if (alarmDays_set > 6) {
        alarmDays_set = 0;
        settingChange++;
      }
      alarmState = false;
      alarmDisabled = false;
      return;
    case 6:
      month += delta;
      if (month < 1) {
        month = 12;
      } else if (month > 12) {
        month = 1;
      }
      break;
    case 7:
      year += delta;
      if (year < 1) {
        year = 9999; // I think it's reasonable to expect this design to be obsolete before then.
      } else if (year > 9999) {
        year = 1; // No BCE dates
      }
      break;
    case 8:
      if (delta == 1) {
        showWeekday = true;
      } else {
        showWeekday = false;
      }
      return;
    case 9:
      dateFmt += delta;
      if (dateFmt < 0) {
        dateFmt = 5;
      } else if (dateFmt > 5) {
        dateFmt = 0;
      }
      return;
    case 10:
      if (delta < 0) {
        if (wakeTime <= 0) {
          wakeTime = sleepTimePreset[8];
        } else {
          for (int i = 8; i > -1; i--) {
            if (sleepTimePreset[i] < wakeTime) {
              wakeTime = sleepTimePreset[i];
              return;
            }
          }
        }
      } else if (delta > 0) {
        if (wakeTime >= sleepTimePreset[8]) {
          wakeTime = 0;
        } else {
          for (int i = 0; i < 9; i++) {
            if (sleepTimePreset[i] > wakeTime) {
              wakeTime = sleepTimePreset[i];
              return;
            }
          }
        }
      }
      return;
    case 14:
      waterThreshold += delta;
      if (waterThreshold < 1) {
        waterThreshold = 16;
      } else if (waterThreshold > 16) {
        waterThreshold = 1;
      }
      return;
    case 15:
      backlightColor += delta;
      if (backlightColor < 0) {
        backlightColor = 7;
      } else if (backlightColor > 7) {
        backlightColor = 0;
      }
      writeBacklight();
      return;
    case 127:
      if (factoryResetSelection == 1) {
        factoryResetSelection = 0;
      } else {
        factoryResetSelection = 1;
      }
      return;
		case 1:
			hour += delta;
			if (hour < 0) {
				hour = 23;
			} else if (hour > 23) {
				hour = 0;
			}
			break;
		case 2:
			minute += delta;
			if (minute < 0) {
				minute = 59;
			} else if (minute > 59) {
				minute = 0;
			}
			break;
		case 3:
			second += delta;
			if (second < 0) {
				second = 59;
			} else if (second > 59) {
				second = 0;
			}
			break;
		case 4:
			if (delta == 1) {
				time12hr = true;
			} else {
				time12hr = false;
			}
			return;
		case 5:
			int dmax = 31;
			switch (month) {
				case 2:
					dmax = (year % 4 == 0) ? 29 : 28; // Feb with leap years
					break;
				case 4:
				case 6:
				case 9:
				case 11:
					dmax = 30;
					break;
				default:
					break;
			}
			day += delta;
			if (day < 1) {
				day = dmax;
			} else if (day > dmax) {
				day = 0;
			}
			break;
		default:
			return;
	}
  DateTime dt_new(year, month, day, hour, minute, second);
  rtc.adjust(dt_new);
}

/**
 * Turns on LCD. Currently not used.
 */
void wake() {
	digitalWrite(13, HIGH);
	secSinceLastEnter = 0;
	lcd.on();
}

/**
 * Changes backlight color, currently not used.
 */
void writeBacklight() {
	analogWrite(LCD_R, colors[backlightColor][0]);
	analogWrite(LCD_G, colors[backlightColor][1]);
	analogWrite(LCD_B, colors[backlightColor][2]);
}

/**
 * Updates the LCD display with date/time and settings if they are being changed.
 */
void updateDisplay() {
  DateTime now = rtc.now();
    
  hour = now.hour();
  minute = now.minute();
  second = now.second();

  int hr_12h = (hour % 12 == 0) ? 12 : hour % 12;

  month = now.month();
  day = now.day();
  year = now.year();
  
  char time_buffer[21];
  char date_buffer[21];
  char frstring[21];
  char al_buffer[21];
  char al_days_buffer[21];
  char wt_buffer[21];

  if (settingChange == 127) {
    lcd.clear();
    lcd.print("   Factory Reset?   ");
    lcd.setCursor(0, 1);
    if (factoryResetSelection == 0) {   // TODO doesn't work
      strncpy(frstring, "      YES  >NO<     ", 21);
    } else {
      strncpy(frstring, "     >YES<  NO      ", 21);
    }
    lcd.print(frstring);
  } else if (time12hr) {
    sprintf(time_buffer, "     %02d:%02d:%02d %s    ", hr_12h, minute, second, (hour > 11) ? "PM" : "AM");

    // Flashing indicators for changing time settings
    if (settingChange == 1 && clockRefreshCounter < 5) {
      time_buffer[5] = ' ';
      time_buffer[6] = ' ';
    } else if (settingChange == 2 && clockRefreshCounter < 5) {
      time_buffer[8] = ' ';
      time_buffer[9] = ' ';
    } else if (settingChange == 3 && clockRefreshCounter < 5) {
      time_buffer[11] = ' ';
      time_buffer[12] = ' ';
    } else if (settingChange == 4 && clockRefreshCounter < 5) {
      time_buffer[14] = ' ';
      time_buffer[15] = ' ';
    }
  } else {
    sprintf(time_buffer, "      %02d:%02d:%02d      ", hour, minute, second);

    // Flashing indicators for changing time settings
    if (settingChange == 1 && clockRefreshCounter < 5) {
      time_buffer[6] = ' ';
      time_buffer[7] = ' ';
    } else if (settingChange == 2 && clockRefreshCounter < 5) {
      time_buffer[9] = ' ';
      time_buffer[10] = ' ';
    } else if (settingChange == 3 && clockRefreshCounter < 5) {
      time_buffer[12] = ' ';
      time_buffer[13] = ' ';
    } else if (settingChange == 4 && clockRefreshCounter < 5) {
      time_buffer[15] = '*';
      time_buffer[16] = 'M';
    }
  }
  if (settingChange != 127) {
    snprintf(date_buffer, 20, dateFormats[dateFmt], (showWeekday) ? daysOfTheWeek_Short[now.dayOfTheWeek()] : "   ", month, day, year);
  //      Serial.print(fsrVal);
    if (dateFmt == 0) {
      // Flashing indicators for changing time settings
      if (settingChange == 6 && clockRefreshCounter < 5) {
        date_buffer[7] = ' ';
        date_buffer[8] = ' ';
      } else if (settingChange == 5 && clockRefreshCounter < 5) {
        date_buffer[10] = ' ';
        date_buffer[11] = ' ';
      } else if (settingChange == 7 && clockRefreshCounter < 5) {
        date_buffer[13] = ' ';
        date_buffer[14] = ' ';
        date_buffer[15] = ' ';
        date_buffer[16] = ' ';
      }
    }
    Serial.print(time_buffer);
    Serial.println(date_buffer);
  
    lcd.clear();
    lcd.print(time_buffer);
    lcd.setCursor(0, 1);
    lcd.print(date_buffer);

    if (settingChange == 11 || settingChange == 12) {
      snprintf(al_buffer, 21, "ALARM   %02d:%02d       ", alarmHr, alarmMin);
      if (settingChange == 11 && clockRefreshCounter < 5) {
        al_buffer[8] = ' ';
        al_buffer[9] = ' ';
      } else if (settingChange == 12 && clockRefreshCounter < 5) {
        al_buffer[11] = ' ';
        al_buffer[12] = ' ';
      }
      lcd.setCursor(0, 2);
      lcd.print(al_buffer);
    } else if (settingChange == 13) {
      // oh dear lord
      // alarm day of week selection string build, probably the most inefficient way to do this, but in blinks when you're about to change it so that's cool
      snprintf(al_days_buffer, 21, "ALARM  %s%s%s%s%s%s%s      ", (alarmDays_mask & 0b1 == 0b1 || (alarmDays_set == 0 && clockRefreshCounter < 5)) ? "S" : "_", 
      (alarmDays_mask & 0b10 == 0b10 || (alarmDays_set == 1 && clockRefreshCounter < 5)) ? "M" : "_", (alarmDays_mask & 0b100 == 0b100 || (alarmDays_set == 2 && clockRefreshCounter < 5)) ? "T" : "_", 
      (alarmDays_mask & 0b1000 == 0b1000 || (alarmDays_set == 3 && clockRefreshCounter < 5)) ? "W" : "_", (alarmDays_mask & 0b10000 == 0b10000 || (alarmDays_set == 4 && clockRefreshCounter < 5)) ? "H" : "_", 
      (alarmDays_mask & 0b100000 == 0b100000 || (alarmDays_set == 5 && clockRefreshCounter < 5)) ? "F" : "_", (alarmDays_mask & 0b1000000 == 0b1000000 || (alarmDays_set == 6 && clockRefreshCounter < 5)) ? "S" : "_");
      
      lcd.setCursor(0, 2);
      lcd.print(al_days_buffer);
    } else if (settingChange == 14) {
      snprintf(wt_buffer, 21, "WATER   %02d oz       ", waterThreshold);
      lcd.setCursor(0, 2);
      lcd.print(wt_buffer);
    }
  }
}

/**
 * Resets settings back to factory defaults.
 * // TODO Probably should just make this wipe RTC NVRAM and reset the sketch.
 */
void factoryReset() {
  // TODO store values in RTC NVRAM and clear on factory reset
	alarmHr = 12;
	alarmMin = 0;
	waterThreshold = 8;
	backlightColor = 2;

	wakeTime = 0;
  timeAwake = 0;

	alarmDays_mask = 0;
	time12hr = true;
	showWeekday = true;

 alarmState = false;

	dateFmt = 0;

	clockRefreshCounter = 0;
	secSinceLastEnter = 0;
	timeAwake = 0;

	factoryResetSelection = 0;

	settingChange = 0;	

	hour = 0;
	minute = 0;
	second = 0;
	day = 1;
	month = 1;
	year = 1970;
	rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
}

/**
 * Handle button presses for changing settings.
 */
void handleButtons() {
  int plusVal = digitalRead(PLUS_BTN);
  int minusVal = digitalRead(MINUS_BTN);
  int enterVal = digitalRead(ENTER_BTN);
  
  if (enterVal == HIGH && minusVal == HIGH && plusVal == HIGH) {
    wake();
    settingChange = 0;
    plus_lock++;
    minus_lock++;
    enter_lock++;
  } else if (minusVal == HIGH && plusVal == HIGH) {
    wake();
    minus_lock++;
    plus_lock++;
    if (timeAwake > 1 && settingChange == 0 && minus_lock > 20 && plus_lock > 20) {
      settingChange = 127; // Prompt factory reset
    }
  } else if (enterVal == HIGH) {    
//    Serial.print("TA:");
//    Serial.print(timeAwake);
//    Serial.print(" EL:");
//    Serial.println(enter_lock);
    wake();
    if (timeAwake > 1 && enter_lock == 0) {
      if (factoryResetSelection == 1 && settingChange == 127) {
        factoryReset();
      } else if (factoryResetSelection == 0 && settingChange == 127) {
        settingChange = 0;
      } else {
        settingChange += 1;
        alarmDays_set = 0;
        if (settingChange == 8) {
          settingChange = 11;  // Skip unimplemented settings
        } else if (settingChange > 14) {
          settingChange = 0;
        }
        Serial.println(settingChange);
      }
    }
    enter_lock++;
  } else if (plusVal == HIGH) {
    wake();
    if (timeAwake > 1 && settingChange > 0) {
      delay(100); // Slow down polling; allow button to be held for fast changes, but not too fast
      clockRefreshCounter++;
      changeSetting(settingChange, 1);
      updateDisplay();
    }
    plus_lock++;
  } else if (minusVal == HIGH) {
    wake();
    if (timeAwake > 1 && settingChange > 0) {
      delay(100); // Slow down polling; allow button to be held for fast changes, but not too fast
      clockRefreshCounter++;
      changeSetting(settingChange, -1);
      updateDisplay();
    }
    minus_lock++;
  } else {
    digitalWrite(13, LOW);
  }

  // Mark buttons released
  if (enterVal == LOW) {
    enter_lock = 0;
  }

  if (plusVal == LOW) {
    plus_lock = 0;
  }

  if (minusVal == LOW) {
    minus_lock = 0;
  }
}
