#include <Wire.h>

// https://github.com/jenschr/Arduino-libraries/tree/master/LiquidCrystal
#include <LiquidCrystal_SR.h>
#include <RTClib.h>

#define PLUS_BTN 10
#define MINUS_BTN 9
#define ENTER_BTN 8

#define BUZZER 7

#define LCD_BACKLIGHT 12 // Used to power on/off LCD backlight Common anode using transistor
#define LCD_R 15 // Software PWM driver pins
#define LCD_G 16
#define LCD_B 17

#define FSR A1

void wake();
void changeSetting(int, int);
void writeBacklight();
void updateDisplay();
void factoryReset();
void handleButtons();
